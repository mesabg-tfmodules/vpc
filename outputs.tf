output "vpc" {
  value       = aws_vpc.vpc
  description = "VPC"
}

output "subnet" {
  value = {
    public = aws_subnet.subnet_public
    private = aws_subnet.subnet_private
  }
  description = "Created Subnets"
}

output "security_group" {
  value = {
    default       = data.aws_security_group.default
    load_balancer = aws_security_group.security_group_alb
    ssh           = aws_security_group.security_group_ssh
  }
  description     = "Created Security Groups"
}
