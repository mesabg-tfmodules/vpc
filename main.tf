terraform {
  required_version = ">= 0.13.2"
}

locals {
  alb = {
    ingress = [
      {
        port        = 80
        description = "HTTP"
      },
      {
        port        = 443
        description = "HTTPS"
      }
    ]
  }
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id        = aws_vpc.vpc.id

  tags = {
    Name        = var.name.full
    Environment = var.environment
  }
}

resource "aws_eip" "eip" {
  domain        = "vpc"
  depends_on    = [aws_internet_gateway.internet_gateway]

  tags = {
    Name        = var.name.full
    Environment = var.environment
  }
}

resource "aws_vpc" "vpc" {
  cidr_block            = var.vpc.cidr
  enable_dns_support    = true
  enable_dns_hostnames  = true

  tags = {
    Name        = var.name.full
    Environment = var.environment
  }
}

resource "aws_subnet" "subnet_public" {
  for_each          = {for net in var.subnet.public:  net.name => net}
  vpc_id            = aws_vpc.vpc.id
  availability_zone = each.value.zone
  cidr_block        = each.value.cidr

  tags = {
    Name            = each.value.name
    Environment     = var.environment
  }
}

resource "aws_subnet" "subnet_private" {
  for_each          = {for net in var.subnet.private:  net.name => net}
  vpc_id            = aws_vpc.vpc.id
  availability_zone = each.value.zone
  cidr_block        = each.value.cidr

  tags = {
    Name            = each.value.name
    Environment     = var.environment
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  count         = length(var.subnet.private) != 0 ? 1 : 0
  allocation_id = aws_eip.eip.id
  subnet_id     = values(aws_subnet.subnet_public)[0].id
  depends_on    = [aws_internet_gateway.internet_gateway]

  tags = {
    Name        = var.name.full
    Environment = var.environment
  }
}

resource "aws_default_route_table" "route_table_public" {
  default_route_table_id  = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name        = "Public"
    Environment = var.environment
  }
}

resource "aws_route_table_association" "route_table_association_public" {
  for_each       = aws_subnet.subnet_public
  subnet_id      = each.value.id
  route_table_id = aws_default_route_table.route_table_public.id
}

resource "aws_route_table" "route_table_private" {
  count             = length(var.subnet.private) != 0 ? 1 : 0
  vpc_id            = aws_vpc.vpc.id

  route {
    cidr_block      = "0.0.0.0/0"
    nat_gateway_id  = aws_nat_gateway.nat_gateway[0].id
  }

  tags = {
    Name            = "Private"
    Environment     = var.environment
  }
}

resource "aws_route_table_association" "route_table_association_private" {
  for_each       = aws_subnet.subnet_private
  subnet_id      = each.value.id
  route_table_id = aws_route_table.route_table_private[0].id
}

resource "aws_security_group" "security_group_alb" {
  name        = "load-balancer"
  description = "Load Balancer HTTPS-HTTP traffic"
  vpc_id      = aws_vpc.vpc.id

  dynamic "ingress" {
    for_each = local.alb.ingress
    iterator = rule

    content {
      description = rule.value.description
      from_port   = rule.value.port
      to_port     = rule.value.port
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "Load Balancer"
    Environment = var.environment
  }
}

resource "aws_security_group" "security_group_ssh" {
  name        = "ssh"
  description = "Allow SSH access to Bastion"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "SSH"
    Environment = var.environment
  }
}

resource "aws_default_security_group" "default" {
  vpc_id      = aws_vpc.vpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  dynamic "ingress" {
    for_each = local.alb.ingress
    iterator = rule

    content {
      description = rule.value.description
      from_port   = rule.value.port
      to_port     = rule.value.port
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  ingress {
    description = "Internal Traffic"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [var.vpc.cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "Default"
    Environment = var.environment
  }
}

data "aws_security_group" "default" {
  name    = "default"
  vpc_id  = aws_vpc.vpc.id
}
