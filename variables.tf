variable "environment" {
  type = string
  description = "Environment name"
}

variable "name" {
  type = object({
    slug = string
    full = string
  })
  description = "General project name."
}

variable "vpc" {
  type = object({
    cidr = string
  })
  description = "VPC description."
}

variable "subnet" {
  type = object({
    public = list(
      object({
        name = string
        zone = string
        cidr = string
      })
    )
    private = list(
      object({
        name = string
        zone = string
        cidr = string
      })
    )
  })
  description = "Subnet configuration."
}
