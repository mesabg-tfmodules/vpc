# VPC Module

This module is capable to generate a full VPC (dual public/private).

Module Input Variables
----------------------

- `environment` - environment name
- `name` - slug and full name
- `vpc` - VPC cidr
- `subnet` - Public and Private subnet description

Usage
-----

```hcl
module "vpc" {
  source      = "git::https://gitlab.com/mesabg-tfmodules/vpc.git"

  environment = "environment"

  name = {
    slug      = "slugname"
    full      = "Full Name"
  }

  vpc = {
    cidr      = "10.0.0.0/16"
  }

  subnet = {
    public = [
      {
        name  = "Public_Subnet_A"
        zone  = "eu-west-2a"
        cidr  = "10.0.0.0/24"
      },
      {
        name  = "Public_Subnet_B"
        zone  = "eu-west-2b"
        cidr  = "10.0.2.0/24"
      }
    ]

    private = [
      {
        name  = "Private_Subnet_A"
        zone  = "eu-west-2a"
        cidr  = "10.0.1.0/24"
      },
      {
        name  = "Private_Subnet_B"
        zone  = "eu-west-2b"
        cidr  = "10.0.3.0/24"
      }
    ]
  }
}
```

Outputs
=======

 - `vpc` - VPC Created description
 - `subnet` - Subnets Created (a, b, private and public)
 - `security_group` - Security Groups Created (default, ssh, lb)
 - `namespace_id` - CloudMap namespace identifier


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
